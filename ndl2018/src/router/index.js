import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import Drone from '@/components/Drone'
import ARView from '@/components/ARView'
import ARJunior from '@/components/ARJunior'
import GoogleMap from '@/components/GoogleMap'
import * as VueGoogleMaps from 'vue2-google-maps'
import CheckListGlobal from '@/components/CheckListGlobal'
import Specification from '@/components/Specification'
import CheckListStartMission from '@/components/CheckListStartMission'
import AlertsAndNotifications from '@/components/AlertsAndNotifications'
import CheckMaterial from '@/components/CheckMaterial'
import Meteo from '@/components/Meteo'
import CheckUp from '@/components/CheckUp'

Vue.use(VueGoogleMaps, {
  load: {
    // Google API key
    key: 'AIzaSyDJ5RhbW5jtMp26-rw6oahpr6bclojitYE',
    // Enable more Google Maps libraries here
    libraries: 'places,drawing,visualization'
  }
})

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/drone',
      name: 'Drone',
      component: Drone
    },
    {
      path: '/ar',
      name: 'ARView',
      component: ARView
    },
    {
      path: '/arj',
      name: 'ARJunior',
      component: ARJunior
    },
    {
      path: '/GM',
      name: 'GoogleMap',
      component: GoogleMap
    },
    {
      path: '/startmission',
      name: 'CheckListStartMission',
      component: CheckListStartMission
    },
    {
      path: '/CLGlobal',
      name: 'CheckListGlobal',
      component: CheckListGlobal
    },
    {
      path: '/Specification',
      name: 'Specification',
      component: Specification
    },
    {
      path: '/alertsAndNotifications',
      name: 'AlertsAndNotifications',
      component: AlertsAndNotifications
    },
    {
      path: '/material',
      name: 'CheckMaterial',
      component: CheckMaterial
    },
    {
      path: '/meteo',
      name: 'Meteo',
      component: Meteo
    },
    {
      path: '/CheckUp',
      name: 'CheckUp',
      component: CheckUp
    }
  ]
})
