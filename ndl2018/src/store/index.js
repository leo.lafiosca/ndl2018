import Vue from 'vue'
import Vuex from 'vuex'
import alert from './modules/alert'
import clglobal from './modules/checkListGlobale'
import clmission from './modules/checkListMission'
import health from './modules/health'
import gMaps from './modules/googleMaps'
import checkmaterial from './modules/checkMaterial'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    alert,
    clglobal,
    clmission,
    health,
    gMaps,
    checkmaterial
  }
})
