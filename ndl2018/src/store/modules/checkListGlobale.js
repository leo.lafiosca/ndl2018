import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  selected: [],
  items: [
    {
      isDisplay: true,
      title: 'Réparation module communication',
      priority: 'Priorité : Important',
      date: 'Date émission : 06/12/2018',
      description: 'Description : Ressouder le cable d\'alimentation du module'
    },
    {
      isDisplay: true,
      title: 'Exploration zone C5',
      priority: 'Priorité : Faible',
      date: 'Date émission : 06/12/2018',
      description: 'Description : Identifier roches présentes sur la zone'
    },
    {
      isDisplay: true,
      title: 'Analyse echantillon 10',
      priority: 'Priorité : Moyen',
      date: 'Date émission : 07/12/2018',
      description: 'Description : Analyser l\'echantillon et envoyer les résultats'
    }
  ]
}

const getters = {
  selectedGlobal: state => state.selected,
  itemsGlobal: state => state.items
}

export default {
  state,
  getters
}
