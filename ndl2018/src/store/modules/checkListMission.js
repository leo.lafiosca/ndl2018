import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  selected: [],
  items: [
    {
      title: 'Téléphone chargé',
      subtitle: 'La charge de votre téléphone est-elle suffisante?'
    },
    {
      title: 'Assez de vivre ?',
      subtitle: 'Avez-vous pris assez de vivre pour le temps de votre mission?'
    },
    {
      title: 'Assez d\'eau ?',
      subtitle: 'Avez-vous pris assez d\'eau pour le temps de votre mission?'
    },
    {
      title: 'Vérification de votre matériel techinique',
      subtitle: 'Avez-vous vérifier les différents matériels nécessaire à votre mission?'
    },
    {
      title: 'Localisation activée',
      subtitle: 'Avez-vous activé la localisation sur votre téléphone?'
    }
  ]
}

const getters = {
  selected: state => state.selected,
  itemsMission: state => state.items
}

export default {
  state,
  getters
}
