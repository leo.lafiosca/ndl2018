import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  items: [
    {
      isDisplay1: true,
      isDisplay2: true,
      title: 'Module communication',
      etat: 'État : A réparer',
      freq: 'Fréquence de vérification : 2 fois par semaine',
      objet1: 'Télephone',
      objet2: 'Chargeur'
    },
    {
      isDisplay1: true,
      isDisplay2: true,
      title: 'Quantité d\'eau',
      etat: 'État : Normal',
      freq: 'Fréquence de vérification : 1 fois par jour',
      objet1: 'Reservoir',
      objet2: 'Collecteur'
    },
    {
      isDisplay1: true,
      isDisplay2: true,
      title: 'Analyseur d\'echantillons',
      etat: 'État : Bon',
      freq: 'Fréquence de vérification : 1 fois par semaine',
      objet1: 'Capteur',
      objet2: 'Recepteur'
    }
  ]
}

const getters = {
  itemsMaterials: state => state.items
}

export default {
  state,
  getters
}