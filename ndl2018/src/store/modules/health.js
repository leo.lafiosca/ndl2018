import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  Pulse: {
    columns: ['date', 'Battement/min'],
    rows: [
      { 'date': '03-12', 'Battement/min': 79 },
      { 'date': '04-12', 'Battement/min': 81 },
      { 'date': '05-12', 'Battement/min': 100 },
      { 'date': '06-12', 'Battement/min': 75 },
      { 'date': '07-12', 'Battement/min': 75 },
      { 'date': '08-12', 'Battement/min': 82 }
    ]
  },
  BloodComp: {
    columns: ['type', '%'],
    rows: [
      { 'type': 'globule blanc', '%': 4 },
      { 'type': 'globule rouge', '%': 41 },
      { 'type': 'plasma', '%': 55 }
    ]
  },
  Temperature: {
    columns: ['date', 'Temperature(°C)'],
    rows: [
      { 'date': '03-12', 'Temperature(°C)': 37.5 },
      { 'date': '04-12', 'Temperature(°C)': 38 },
      { 'date': '05-12', 'Temperature(°C)': 41 },
      { 'date': '06-12', 'Temperature(°C)': 37 },
      { 'date': '07-12', 'Temperature(°C)': 37.2 },
      { 'date': '08-12', 'Temperature(°C)': 37.5 }
    ]
  }
}

const getters = {
  Pulse: state => state.Pulse,
  BloodComp: state => state.BloodComp,
  Temperature: state => state.Temperature
}

export default {
  state,
  getters
}
