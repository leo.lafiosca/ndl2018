import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  modulesAdded: [],
  modules: [
    ""
  ]
}

const getters = {
  modulesAdded: state => state.modulesAdded,
  modules: state => state.modules
}

const mutations = {
  ADD_MODULES (state, module) {
	  state.modulesAdded.push(module)
  }
}

const actions = {
  addModules ({commit}, module) {
	commit('ADD_MODULES', module)
  }
}

export default {
  state,
  getters
}
