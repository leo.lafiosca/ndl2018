import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  center: { lat: 45.508, lng: -73.587 },
  markers: [],
  places: [],
  currentPlace: null
}

const getters = {
  center: state => state.center,
  markers: state => state.markers,
  places: state => state.places,
  currentPlace: state => state.currentPlace
}

const mutations = {
  ADD_MARKERS (state, marker) {
    state.markers.push(marker)
  },
  ADD_PLACES (state, place) {
    state.places.push(place)
  },
  SET_CENTER (state, center) {
    state.center = center
  },
  SET_CURRENT_PLACE (state, place) {
    state.currentPlace = place
  }
}

const actions = {
  addMarker ({commit}, marker) {
    commit('ADD_MARKERS', marker)
  },
  addPlace ({commit}, place) {
    commit('ADD_PLACES', place)
  },
  setCenter ({commit}, center) {
    commit('SET_CENTER', center)
  },
  setCurrentPlace ({commit}, place) {
    commit('SET_CURRENT_PLACE', place)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
