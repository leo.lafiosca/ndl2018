import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  messages: [
    {
      header: 'Today',
      data: [
        {
          color: 'blue',
          icon: 'notification_important',
          name: 'Nouvelle tâche ajoutée par vos supérieurs'
        },
        {
          color: 'red',
          icon: 'warning',
          name: 'Chaleur extrème !'
        },
        {
          color: 'red',
          icon: 'warning',
          name: 'Vent violent !'
        },
        {
          color: 'blue',
          icon: 'notification_important',
          name: 'Batterie du drône faible'
        }
      ]
    },
    {
      header: 'Yesterday',
      data: [
        {
          color: 'blue',
          icon: 'notification_important',
          name: 'Nouvelle tâche ajoutée par vos supérieurs'
        },
        {
          color: 'red',
          icon: 'warning',
          name: 'Chaleur extrème !'
        }
      ]
    }
  ],
  lorem: 'Lorem ipsum dolor sit amet, at aliquam vivendum vel, everti delicatissimi cu eos. Dico iuvaret debitis mel an, et cum zril menandri. Eum in consul legimus accusam. Ea dico abhorreant duo, quo illum minimum incorrupte no, nostro voluptaria sea eu. Suas eligendi ius at, at nemore equidem est. Sed in error hendrerit, in consul constituam cum.'
}

const getters = {
  messages: state => state.messages,
  lorem: state => state.lorem
}

export default {
  state,
  getters
}
