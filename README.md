# Nuit de l'info 2018

### Error 418

Laetitia Bourdon  
Lucie Collion  
Adrian Amaglio  
Corentin Chay  
Christian Jimenez  
Olivier Pradelle  
Léo Lafiosca  

#### Voici notre rendus pour la nuit de l'info 2018.  

## Site

Notre site peut être consulter ici : [https://51.75.90.158/#/](https://51.75.90.158/#/)  

## Features 

### Alertes et notifications

Consultez vos alertes et les notifications envoyez par la base!  

### Liste des missions

Toutes vos tâches à faire/faites  

### Commencer une mission

Avant de partir en mission, assurez vous d'avoir bien vérifié quelques points essentiels.  

Ensuite, laissez vous guidez vers votre objectif par une flèche en réalitée augmentée avec votre camera (à utiliser sur téléphone, fonctionne sous Chrome).  

### Vérification du matériel

Vérifiez l'état de votre materiel.  

### Bilan de santé

Consultez votre bilan de santé!  

### Exploration avec le drone

Prennez le controle de votre drone et explorez les environs depuis votre ordinateur, tablette ou smartphone!  

### Météo

La météo de la semaine! Il fait chaud dans le désert du Namib...  

### Mode nuit

Changez la couleur de fond de l'appli en cas de forts contrastes.

### Position

Localisez vous et vos objectifs sur une carte!

### Personalisation de l'affichage

Sellectionnez les modules à afficher dans le menu!
